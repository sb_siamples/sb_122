package siample.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sb122Application {

	public static void main(String[] args) {
		SpringApplication.run(Sb122Application.class, args);
	}

}
