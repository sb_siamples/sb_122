package siample.dev.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import siample.dev.domain.Story;
import siample.dev.repo.StoryRepository;
import siample.dev.service.StoryService;

@Controller
public class HomeController {
	
	@Autowired
	StoryService storyService;
	
	@RequestMapping("/")
	public String index(Model model) {
		model.addAttribute("stories", getStories());
		return "stories";
	}
	
	@RequestMapping("/story")
	public String idxone(Model model) {
		model.addAttribute("story", getStory());
		return "story";
	}

	@RequestMapping("/title/{title}")
	public String searchForStoryByTitle(@PathVariable("title") String title, Model model) throws Exception {
		if (title==null) {
			throw new Exception("Ilyen című sztori nem található!");
		} // lehet ezt inkább a service-ben kéne lekezelni.
		model.addAttribute("story", getStoryByTitle(title));
		return "story";
	}
	
	private Story getStoryByTitle(String title) {
		return storyService.getSpecificStory(title);
	}

	private Story getStory() {
		return storyService.getStory();
	}

	private List<Story> getStories() {
		//List<Story> stories = storyService.;
		return storyService.getStories();
	}
	

}
