package siample.dev.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import siample.dev.domain.Blogger;

public interface BloggerRepository extends CrudRepository<Blogger, Long> {
	
	List<Blogger> findAll(); // not iterator
	// JDBC háttérben ezt futtatja: SELECT * FROM blogger; 

}
