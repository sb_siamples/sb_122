package siample.dev.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import siample.dev.domain.Story;

public interface StoryRepository extends CrudRepository<Story, Long> {
	
	List<Story> findAll(); // not Iterable
	// JDBC háttérben futtatja a SELECT * FROM story; sql-t.

	Story findFirstByOrderByPostedDesc();// Desc legújabb 
	// SELECT * FROM story WHERE posted IN (select max(posted) from story) LIMIT 1;
	
	Story findByTitle(String title);
	
}
