package siample.dev.service;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import siample.dev.domain.Blogger;
import siample.dev.domain.Story;
import siample.dev.repo.BloggerRepository;
import siample.dev.repo.StoryRepository;

@Service
public class StoryService {

	@Autowired
	StoryRepository storyRepo;

	@Autowired
	BloggerRepository bloggerRepo;

	public List<Story> getStories() {
		// init(); // itt minden html-requestre lefut, és újra és újra további Gyula 53
		// / Cím223-as bloggeres story-t hozzáad
		return storyRepo.findAll();
	}

	@PostConstruct // itt csak 1x fut le !
	public void init() {
//		Blogger blogger = new Blogger("Gyula", 53);
//		bloggerRepo.save(blogger);
//		
//		Story story = new Story("Cím223","Monoton ügető süppedő homokon", "HESHe", new Date(), blogger);
//		storyRepo.save(story);

	}

	public Story getStory() {
		// TODO Auto-generated method stub
		return storyRepo.findFirstByOrderByPostedDesc();
	}
	
	public Story getSpecificStory(String title) {
		return storyRepo.findByTitle(title);
	}
}
