insert into Blogger (age,name) VALUES (21, 'Gyula');
insert into Blogger (age,name) VALUES (26, 'Krisz');

--insert into STORY (title,content,posted,blogger_id) values ('Teszt cím' , 'Teszt tartalom',   CURRENT_DATE(), (select id FROM BLOGGER where name = 'Gyula') );
--insert into STORY (title,content,posted,blogger_id) values ('Teszt2 cím', 'Teszt2 tartalom2', CURRENT_DATE(), (select id FROM BLOGGER where name = 'Gyula') );
--insert into STORY (title,content,posted,blogger_id) values ('Krisz cím' , 'Krisz tartalom',   CURRENT_DATE(), (select id FROM BLOGGER where name = 'Krisz') );
--insert into STORY (title,content,posted,blogger_id) values ('Krisz2 cím', 'Krisz2 tartalom2', CURRENT_DATE(), (select id FROM BLOGGER where name = 'Krisz') );
--insert into STORY (title,content,posted,blogger_id) values ('Szia szép',  'Szia tartalom2',   CURRENT_DATE(), (select id FROM BLOGGER where name = 'Krisz') );


-- @Entity(name = "stories") esetén, @Column(name="cim") esetén:
insert into STORIES (author, cim,content,posted,blogger_id) values ('Romhányi',  'Teszt' , 'Teszt tartalom',   CURRENT_DATE(), (select id FROM BLOGGER where name = 'Gyula') );
insert into stories (author, CIM,content,posted,blogger_id) values ('Anonymus',  'Teszt2 cím', 'Teszt2 tartalom2', CURRENT_DATE(), (select id FROM BLOGGER where name = 'Gyula') );
insert into STORIES (author, cim,content,posted,blogger_id) values ('Verdi',     'Krisz cím' , 'Krisz tartalom',   CURRENT_DATE(), (select id FROM BLOGGER where name = 'Krisz') );
insert into stories (author, CIM,content,posted,blogger_id) values ('Rigoletto', 'Krisz2 cím', 'Krisz2 tartalom2', CURRENT_DATE(), (select id FROM BLOGGER where name = 'Krisz') );
insert into STORIES (author, cim,content,posted,blogger_id) values ('Az asszony ingatag','Szia szép', 'Szia tart2', CURRENT_DATE(), (select id FROM BLOGGER where name = 'Krisz') );
